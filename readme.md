# Тестовое задание IDFLY #

====================================================

### Для корректной работы проекта необходимы: ###
1. Bower - для установки компонентов;
2. Gulp - для сборки проекта;
3. JSON-server - для имитации БД.

### Подготовка ###
Устанавливаем Bower

```
#!bash

sudo npm i bower -g
```


Устанавливаем Gulp

```
#!bash

sudo npm i gulp -g
```


Устанавливаем JSON-server

```
#!bash

sudo npm i json-server -g
```

Переходим в папку с проектом и устанавливаем нужные зависимости

```
#!bash

cd ~/path/to/project/
npm i
bower i
```

В новой вкладке/окне терминала запускаем наш JSON-server

```
#!bash

cd ~/path/to/project/json-server
json-server --watch db.json
```

Запускаем сборку и синхронизацию с браузером
```
#!bash

gulp
gulp browser-sync
```
Запустится Chrome с рабочим проектом.
