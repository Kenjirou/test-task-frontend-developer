'use strict';

angular.module('testApp')
  .constant('baseURL', 'http://localhost:3000/')
  .service('requestFactory', ['$resource', 'baseURL', function($resource, baseURL) {

    this.getRequest = function() {
      return $resource(baseURL + 'requests');
    };

  }])

;
