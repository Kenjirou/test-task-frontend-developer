'use strict';

angular.module('testApp', ['ui.router', 'ngResource'])
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

    // route for the tarifs page
    .state('app', {
      url: '/',
      views: {
        'header': {
          templateUrl: 'views/header.html'
        },
        'content': {
          templateUrl: 'views/tarifs.html'
        }
      }

    })

    // route for the request page
    .state('app.request', {
      url: 'request',
      views: {
        'content@': {
          templateUrl: 'views/request.html',
          controller: 'RequestController'
        }
      }
    });

    $urlRouterProvider.otherwise('/');
  });
