'use strict';

angular.module('testApp')

.controller('RequestController', ['$scope', 'requestFactory', function($scope, requestFactory) {
  // array for urgency
  $scope.instancy = ['low', 'normal', 'high'];

  // array of objects for radio buttons in request form
  $scope.projects = [{
    value: 'blog',
    label: 'Blog'
  }, {
    value: 'emarket',
    label: 'E-Market'
  }, {
    value: 'gallery',
    label: 'Gallery'
  }];

  // container for the data from user input in request form
  $scope.request = {
    subject: '',
    text: '',
    urgency: '',
    date: ''
  };

  // variables for showing error when DB is not in a state
  $scope.showTable = true;
  $scope.message = "Loading ...";

  $scope.sendRequest = function() {
    // the date is automatically sent when a form is submitted
    $scope.request.date = new Date().toISOString();

    console.log($scope.request);

    // send the data from the container in the DB
    requestFactory.getRequest().save($scope.request);

    // and reset the form to its original view
    $scope.requestForm.$setPristine();
    $scope.request = {
      subject: '',
      text: '',
      urgency: '',
      date: ''
    };
  };

  $scope.sortType = 'id';
  $scope.sortReverse = false;
  $scope.searchIn = '';

  // logic to output the data from the database
  $scope.requests = requestFactory.getRequest().query(
    function(response) {
      $scope.requests = response;
      $scope.showTable = true;
    },
    function(response) {
      $scope.message = "Error: " + response.status + " " + response.statusText;
    }
  );

  $scope.showProject = function(data) {
    return $scope.searchIn = data;
  };

}])
;
