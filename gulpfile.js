'use strict';

var gulp = require('gulp'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  minifycss = require('gulp-minify-css'),
  jshint = require('gulp-jshint'),
  stylish = require('jshint-stylish'),
  uglify = require('gulp-uglify'),
  usemin = require('gulp-usemin'),
  imagemin = require('gulp-imagemin'),
  notify = require('gulp-notify'),
  cache = require('gulp-cache'),
  rev = require('gulp-rev'),
  browserSync = require('browser-sync'),
  ngannotate = require('gulp-ng-annotate'),
  del = require('del'),
  autoprefixerOptions = {
  browsers:
    [
      'last 2 versions',
      '> 5%',
      'Firefox ESR'
    ]
  }
;

gulp.task('jshint', function() {
  return gulp.src('assets/scripts/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});

gulp.task('sass', function () {
  return gulp
    .src('assets/styles/*.scss')
    .pipe(sass({outputStyle: 'compressed'})
    .on('error', sass.logError))
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest('assets/styles'));
});

// Clean
gulp.task('clean', function() {
  return del(['build']);
});

// Default task
gulp.task('default', ['clean'], function() {
  gulp.start('usemin', 'imagemin', 'copyfonts');
});

gulp.task('usemin', ['jshint'], function() {
  return gulp.src('assets/**/*.html')
    .pipe(usemin({
      css: [minifycss(), rev()],
      js: [ngannotate(), uglify(), rev()]
    }))
    .pipe(gulp.dest('build/'));
});

// Images
gulp.task('imagemin', function() {
  return del(['build/images']), gulp.src('assets/images/**/*')
    .pipe(cache(imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest('build/images'))
    .pipe(notify({
      message: 'Images task complete'
    }));
});

gulp.task('copyfonts', ['clean'], function() {
  gulp.src('./bower_components/font-awesome/fonts/**/*.{ttf,woff,eof,svg}*')
    .pipe(gulp.dest('./build/fonts'));
  gulp.src('./bower_components/bootstrap/dist/fonts/**/*.{ttf,woff,eof,svg}*')
    .pipe(gulp.dest('./build/fonts'));
});

// Watch
gulp.task('watch', ['browser-sync'], function() {
  gulp.watch('assets/styles/**/*.scss', ['sass']);
  // Watch .js files
  gulp.watch('{assets/scripts/**/*.js,assets/styles/**/*.css,assets/**/*.html}', ['usemin']);
  // Watch image files
  gulp.watch('assets/images/**/*', ['imagemin']);

});

gulp.task('browser-sync', ['default'], function() {
  var files = [
    'assets/**/*.html',
    'assets/styles/**/*.css',
    'assets/images/**/*.png',
    'assets/scripts/**/*.js',
    'build/**/*'
  ];

  browserSync.init(files, {
    server: {
      baseDir: "build",
      index: "index.html"
    }
  });
  // Watch any files in build/, reload on change
  gulp.watch(['build/**']).on('change', browserSync.reload);
});
